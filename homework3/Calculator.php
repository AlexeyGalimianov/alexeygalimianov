<?php
/**
 * Created by PhpStorm.
 * User: aleksej
 * Date: 15.04.18
 * Time: 16:19
 */

class Calculator
{
    protected $first, $second;

    function __construct($first1, $second1)
    {
        $this->first = $first1;
        $this->second = $second1;
        echo 'a = ' . $first1. ', ' . 'b = ' . $second1 . '<br>';
    }


    public function add()
    {
        return $this->first + $this->second;
    }
    public function substract()
    {
        return $this->first - $this->second;
    }
    public function multiply()
    {
        return $this->first * $this->second;
    }
    public function divide()
    {
        return $this->first / $this->second;
    }
}
$Calc = new Calculator(90, 7);
echo 'a + b = ' . $Calc -> add()       . '<br>';
echo 'a - b = ' . $Calc -> substract() . '<br>';
echo 'a \ b = ' . $Calc -> divide()    . '<br>';
echo 'a * b = ' . $Calc -> multiply()  . '<br>';


