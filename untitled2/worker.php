<?php
/**
 * Created by PhpStorm.
 * User: aleksej
 * Date: 14.04.18
 * Time: 14:10
 */


class WorkerAlpha
{
    public $name = 'Name';
    public $age = 0;
    public $salary = 100;

}

$worker1 = new WorkerAlpha();

$worker1->name = 'Иван';
$worker1->age= 25;
$worker1->salary= 1000;

$worker2 = new WorkerAlpha();

$worker2->name = 'Вася';
$worker2->age = 26;
$worker2->salary =2000;


echo 'WorkerAlpha c полями public', '<br>';
var_dump('Сумма зарплат Ивана и Васи = ' , $worker1->salary+$worker2->salary);
echo ('<br>');
var_dump('Сумма возрастов Ивана и Васи = ' , $worker1->age+$worker2->age);
echo ('<br>' . '<br>');



class WorkerBetta
{
    private $name = 'Name';
    private $age = 0;
    private $salary = 100;

    public function __get($property)
        {
            return $this->$property;
        }

    public function __set($property, $value)
    {
        $this->$property = $value;
        return $this;
    }
}

    $worker1 = new WorkerBetta();

        $worker1->name = 'Иван';
        $worker1->age= 25;
        $worker1->salary= 1000;

    $worker2 = new WorkerBetta();

        $worker2->name = 'Вася';
        $worker2->age = 26;
        $worker2->salary =2000;


    echo 'WorkerBeta c полями private', '<br>';
    var_dump('Сумма зарплат Ивана и Васи = ' , $worker1->salary+$worker2->salary);
    echo ('<br>');
    var_dump('Сумма возрастов Ивана и Васи = ' , $worker1->age+$worker2->age);
echo ('<br>' . '<br>');

class WorkerOmega
{
    private $name = 'Name';
    private $age = 0;
    private $salary = 10;

    public function __get($property)
    {
        return $this->$property;
    }

    public function __set($property, $value)
    {
        $this->$property = $value;
        return $this;
    }

    function checkAge()
    {
        if($this->age > 100 || $this->age < 1)
            {
                echo ' V A R N I N G ! Введен неправильный возраст : ';
                echo $this->name ,  ', ' , $this->age;
                echo '<br>' . ' Данный возраст установлен к значению 50 ' . '<br>';
                $this->age = 50;
            }
    }

}

$worker1 = new WorkerOmega();

$worker1->name = 'Иван';
$worker1->age= 25;
$worker1->salary= 1000;
$worker1->checkAge();

$worker2 = new WorkerOmega();

$worker2->name = 'Вася';
$worker2->age = 26;
$worker2->salary =2000;
$worker2->checkAge();


echo 'WorkerOmega c полями private и методом checkAge', '<br>';
var_dump('Сумма зарплат Ивана и Васи = ' , $worker1->salary+$worker2->salary);
echo ('<br>');
var_dump('Сумма возрастов Ивана и Васи = ' , $worker1->age+$worker2->age);
echo ('<br>' . '<br>');
